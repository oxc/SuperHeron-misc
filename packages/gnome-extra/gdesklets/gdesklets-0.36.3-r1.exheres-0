# Copyright 2009-2017 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.11 ] ]
require freedesktop-desktop freedesktop-mime
require python [ blacklist=3 multibuild=false ]

SUMMARY="gDesklets - GNOME Desktop Applets"
DESCRIPTION="
gDesklets is a system for bringing mini programs (desklets),
such as weather forecasts, news tickers, system information displays,
or music player controls, onto your desktop, where they are
sitting there in a symbiotic relationship of eye candy and usefulness.
The possibilities are really endless and they are always there to serve
you whenever you need them, just one key-press away. The system is not
restricted to one desktop environment, but currently works on most of
the modern Unix desktops (including GNOME, KDE, Xfce).
"
HOMEPAGE="http://gdesklets.info"
DOWNLOADS="http://archive.gdesklets.info/gDesklets/${PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/expat[>=2.0]
        gnome-bindings/gnome-python[>=2.6.0][bonobo][bonoboui][gnomecanvas]
        gnome-bindings/pygtk:2[>=2.10][python_abis:*(-)?]
        gnome-bindings/pyorbit[>=2.0.1][python_abis:*(-)?]
        gnome-desktop/libgtop:2[>=2.8.0]
        gnome-desktop/librsvg:2[>=2.8.0]
"

DEFAULT_SRC_PREPARE_PATCHES=( -p0 "${FILES}/${PNV}-.in-files.patch" )

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

